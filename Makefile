clean:
	sudo rm -rf www mysql

rules:
	sudo chmod -R 777 www/wp-content/themes
	sudo chmod -R 777 www/wp-content/plugins

www-data:
	sudo chown www-data:www-data www/wp-content/themes/woodmart
	sudo chown www-data:www-data www/wp-content/themes/woodmart-child

backup:
	docker exec krug_db sh -c 'exec /usr/bin/mysqldump -uroot -pfab14france wordpress' > backup.sql

sql:
	docker exec -it krug_db mysql -uroot -pfab14france -Dwordpress

ip:
	docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' krug_ftp
