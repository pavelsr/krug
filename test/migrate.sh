#!/bin/sh

echo -e "${BLUE}List of all running docker containers{NC}, sorted by name: "
docker ps --format "{{.Names}}" | sort | column

read -p "Type the name of your wordpress container : " WP_CONTAINER

curr_domain=$(docker run -it --rm --volumes-from $WP_CONTAINER --network container:$WP_CONTAINER wordpress:cli option get siteurl)
echo "Current address is : $curr_domain"

echo "Enter new <host>:<port> (without http or https prefix) followed by [ENTER]:"
read new_domain

docker run -it --rm --volumes-from $WP_CONTAINER --network container:$WP_CONTAINER wordpress:cli option update home https://$new_domain
docker run -it --rm --volumes-from $WP_CONTAINER --network container:$WP_CONTAINER wordpress:cli option update siteurl https://$new_domain

new_domain_test=$(docker run -it --rm --volumes-from $WP_CONTAINER --network container:$WP_CONTAINER wordpress:cli option get siteurl)
echo "Current address is : $new_domain_test"

echo "Old address $curr_domain is also in tables: "
docker run -it --rm --volumes-from test_wordpress_1 --network container:test_wordpress_1 wordpress:cli search-replace $curr_domain https://$new_domain/ --dry-run

read -r -p "Run migration on tables? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    docker run -it --rm --volumes-from test_wordpress_1 --network container:test_wordpress_1 wordpress:cli search-replace $curr_domain https://$new_domain/
else
    exit 0
fi


# docker run -it --rm --volumes-from test_wordpress_1 --network container:test_wordpress_1 wordpress:cli search-replace localhost https://$new_domain/ --dry-run

# make cli :
# option get siteurl
# option list
# search-replace http://0.0.0.0:32774 https://blog.school.myfablab.ru/ --dry-run
# search-replace 0.0.0.0:32774 https://blog.school.myfablab.ru/ --dry-run

# option update home https://blog.school.myfablab.ru
# option update siteurl https://blog.school.myfablab.ru
