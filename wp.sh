#!/bin/sh

# https://developer.wordpress.org/cli/commands/
# e.g. wp user list

echo "Enter wp command (without wp): "
read wp

WP_CONTAINER="krug"

docker run -it --rm \
  --volumes-from $WP_CONTAINER \
  --network container:$WP_CONTAINER \
  wordpress:cli $wp
