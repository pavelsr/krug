use Text::AutoCSV;
use Data::Dumper;
use Data::Dumper::AutoEncode;
use feature 'say';
use JSON::XS;
use LWP::UserAgent;
use HTTP::Request::Common;
use utf8;

my $ua = LWP::UserAgent->new;
my $wp_base_url = 'localhost:9999';
my $woo = {
	consumerKey => 'ck_9e87e6f9af0d3c403b05cdc6053a3cd919e3907c',
  	consumerSecret => 'cs_1b3ca406f456f88e3c0fad21fd518d2249573466'
};
my $csv = Text::AutoCSV->new(in_file => 'doc/categories.csv', walker_ar => \&walk);
my @root_categories = map { $_->[1] } $csv->get_coldata();
$csv->read();

my $res;

sub walk {
	my $row_ar = shift;
    for my $i (0 .. $#root_categories) {
      $root_categories[$i];
      push @{ $res->{ $root_categories[$i] } }, $row_ar->[$i] if $row_ar->[$i];
	}
}

# sub add_category_2 {
# 	my ($name, $parent_cat) = @_;

# 	my $encoded_data = encode_json { 'name' => $name };
# 	# warn $encoded_data;
# 	my $r = HTTP::Request->new(
# 		'POST', 
# 		'http://'.$wp_base_url.'/wp-json/wc/v2/products/categories', 
# 		[ 'Content-Type'  => 'application/json'],
# 		$encoded_data
# 	);
# 	$r->authorization_basic($woo->{consumerKey}, $woo->{consumerSecret});
# 	warn Dumper $r;
# 	return $ua->request($r)->content;

# 	# my $data = encode_json { 'name' => $name };
# 	# #$ua->credentials($wp_base_url, "realm-name", $woo->{consumerKey}, $woo->{consumerSecret});                     
# 	# my $request = HTTP::Request::Common::POST (
# 	# 	'http://'.$wp_base_url.'/wp-json/wc/v2/products/categories',
# 	# 	[ 'Content-Type'  => 'applic	ation/json',
# 	# 	'Content' => $data ]
# 	# );
# 	# $request->authorization_basic($woo->{consumerKey}, $woo->{consumerSecret});
# 	# my $res = $ua->request($request);

# 	# # my $res = $ua->post( 
# 	# # 	'http://'.$wp_base_url.'/wp-json/wc/v2/products/categories', 
# 	# # 	'Content-Type'  => 'application/json',
# 	# # 	Content => $data
# 	# # );

# 	# $res->content;
# }


sub add_category {

	my ($name, $parent_cat_id) = @_;
	my $cmd = "docker run -it --rm --volumes-from krug-dev --network container:krug-dev wordpress:cli wp wc product_cat create --name=\'".$name."\' --user=1";
	
	$cmd.=' --parent='.$parent_cat_id if $parent_cat_id;

	my $t = `$cmd`;

	# warn $t;

	if ($t =~ /product_cat ([0-9]+)/) {
		return $1;
	}

	return 0;
}



# Add root categories

while( my ($root_cat_name, $subcat_names_arr) = each %$res) { 

	my $parent_id = add_category($root_cat_name);
	say "Category ".$root_cat_name." was added, id: ".$parent_id;

	for my $subcat (@$subcat_names_arr) {
		my $id = add_category($subcat, $parent_id);
		say "Subcategory ".$subcat." was added, id: ".$id;
	}

}

# warn eDumper $res;


# docker run -it --rm --volumes-from krug-dev --network container:krug-dev wordpress:cli wp wc product_cat create --name="$name" --user=1